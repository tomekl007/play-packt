import _root_.io.gatling.sbt.GatlingPlugin
import _root_.sbt.Keys._

name := """play-scala"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .enablePlugins(GatlingPlugin)
  .configs(GTest)
  .settings(inConfig(GTest)(Defaults.testSettings): _*)

scalaVersion := "2.11.8"

libraryDependencies += jdbc
libraryDependencies += cache
libraryDependencies += ws
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test
libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.1.0" % Test
libraryDependencies += "io.gatling" % "gatling-test-framework" % "2.1.0" % Test


lazy val GTest = config("gatling") extend Test

scalaSource in GTest := baseDirectory.value / "/gatling/simulation"
/*In this line we are just configuring to take all the gatling test cases
from the "gatling/simulation" folder of the base directory.*/
