import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._


class MessagesEndpointTest extends Simulation {

  val scn = scenario("My scenario")
    .exec(http("My Page")
      .post("http://localhost:9000/message")
      .body(StringBody( """{ "msg": "This is sample message", "id": 1 }""")).asJSON
    )

  setUp(
    scn.inject(constantUsersPerSec(50) during (2 minutes))
  )
}
