package model

import play.api.libs.json.{JsResult, JsValue, Reads}

case class Message(msg: String, id: Int)

object Message {
  implicit val readsMyClass: Reads[Message] = new Reads[Message] {
    def reads(json: JsValue): JsResult[Message] = {
      for {
        msg <- (json \ "msg").validate[String]
        id <- (json \ "id").validate[Int]
      } yield Message(msg, id)
    }
  }
}