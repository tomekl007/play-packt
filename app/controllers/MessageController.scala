package controllers

import java.util.concurrent.Executors
import javax.inject._

import akka.actor.ActorSystem
import model.{MessageRepository, Message}
import play.api.mvc._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future, Promise}

/**
  * This controller creates an `Action` that demonstrates how to write
  * simple asynchronous code in a controller. It uses a timer to
  * asynchronously delay sending a response for 1 second.
  *
  * @param actorSystem We need the `ActorSystem`'s `Scheduler` to
  *                    run code after a delay.
  * @param exec We need an `ExecutionContext` to execute our
  *             asynchronous code.
  */
@Singleton
class MessageController @Inject()(actorSystem: ActorSystem, messageRepository: MessageRepository)(implicit exec: ExecutionContext) extends Controller {
  implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(32))

  def createMessage = Action { request =>
    val msg = request.body.asJson.get.as[Message]
    messageRepository.saveMessage(msg)
    Ok
  }

  def createMessageAsync = Action { request =>
    Future {
      val msg = request.body.asJson.get.as[Message]
      messageRepository.saveMessage(msg)

    }
    Accepted
  }


  def pingAsync = Action.async {
    getFutureMessage(1.second).map { msg => Ok(msg) }
  }

  private def getFutureMessage(delayTime: FiniteDuration): Future[String] = {
    val promise: Promise[String] = Promise[String]()
    actorSystem.scheduler.scheduleOnce(delayTime) {
      promise.success("Hi!")
    }
    promise.future
  }


}
